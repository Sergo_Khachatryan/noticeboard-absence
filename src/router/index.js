import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/notice-board",
    name: "NoticeBoard",
    component: () => import("@/views/NoticeBoard/NoticeBoard.vue"),
    alias: "/NoticeBoard",

    meta: {
      title: "NoticeBoard",
      layout: "DashboardLayout",
    },
  },
  {
    path: "/",
    component: ()=> import("@/views/AbsenceManagement/AbsenceManagement")
  },
  {
    path: "*",
    component: ()=> import("@/views/PageNotFound")
  }

];

const router = new VueRouter({
  linkActiveClass: "active", // active class for non-exact links.
  linkExactActiveClass: "active", // active class for *exact* links.
  mode: "history",
  routes,
});

export default router;
