const state= {
        notice: [],
        dialogVisible:false,
    };
const getters = {
    getNotice:function (state){
        return state.notice
    },
    getDialogVisible(state){
        return state.dialogVisible
    }
}

const mutations ={
    setNotice(state, value){
        state.notice.push(value)
    },
    updateNotice(state, payload) {
        const item = state.notice.find(i => i.date === payload.date)
        Object.assign(item, payload)
    },
    deleteNotice(state, payload) {
        const index = state.notice.findIndex(i => i.date === payload.date)
        state.notice.splice(index, 1)
    },
    showDialog(state){
        state.dialogVisible = true
    },
    hideDialog(state){
        state.dialogVisible = false

    }
}
const actions={
    setAllNotice({commit},value){
        commit('setNotice',value)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
